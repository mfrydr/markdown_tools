---
title:  'This is the title: it contains a colon'
date: 2017-01-06
author:
- Author One
- Author Two
keywords:
- nothing
- nothingness
abstract: |
  This is the abstract.

  It consists of two paragraphs.
---


# Modern JS (2012, Ullman)
## Part 1

``` {.haskell}
qsort [] = []
```

| The limerick packs laughs anatomical
| In space that is quite economical.
|    But the good ones I've seen
|    So seldom are clean
| And the clean ones so seldom are comical

| 200 Main St.
| Berkeley, CA 94718

#. one
#. two

Term 1

:   Definition
with lazy continuation.

    Second paragraph of the definition.

Term 1
  ~ Definition 1

Term 2
  ~ Definition 2a
  ~ Definition 2b

(@)  My first example will be numbered (1).
(@)  My second example will be numbered (2).

Explanation of examples.

(@)  My third example will be numbered (3).

(@good)  This is a good example.

As (@good) illustrates, ...


-------------------------------------------------------------
 Centered   Default           Right Left
  Header    Aligned         Aligned Aligned
----------- ------- --------------- -------------------------
   First    row                12.0 Example of a row that
                                    spans multiple lines.

  Second    row                 5.0 Here's another one. Note
                                    the blank line between
                                    rows.
-------------------------------------------------------------

Table: Here's the caption. It, too, may span
multiple lines.


: Sample grid table.

+---------------+---------------+--------------------+
| Fruit         | Price         | Advantages         |
+===============+===============+====================+
| Bananas       | $1.34         | - built-in wrapper |
|               |               | - bright color     |
+---------------+---------------+--------------------+
| Oranges       | $2.10         | - cures scurvy     |
|               |               | - tasty            |
+---------------+---------------+--------------------+

```{.python}
for thing in stuff:
    #hahaha
```

### Chapter 1 Intro to JS
### Chapter 2 JS project overview/example
### Chapter 3 Tools
