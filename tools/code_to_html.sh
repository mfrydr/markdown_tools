#!/bin/bash
set -eo pipefail

# Given source code and a css file, generate html
# to display the source code nicely in a web browser.

rel_script_dir="$(dirname $0)"
cssfile="$rel_script_dir/gh_pandoc.css"
# temporary files
footerfile="pandoc_footer.html"

function print_usage {
  echo Usage: "$0 -i <path/to/source> [-o <target.html>] [-c <path/to/css>] [-d <path/to/outdir>]"
  exit 1
}

# need at least one argument, -i option
if [[ $# -lt 2 ]]; then
  print_usage
fi

while [[ $# -gt 1 ]]
do
  key="$1"
  case $key in
      -i|--in)
      sourcefile="$2"
      shift
      ;;
      -o|--out)
        outfile="$2"
        shift
        ;;
      -c|--css)
        cssfile="$2"
        shift
        ;;
      -d|--out-dir)
        outdir="$2"
        shift
        ;;
      -h|--help)
        print_usage
        ;;
      *)
        print_usage
        ;;
  esac
  shift
done

if [[ -z "$sourcefile" ]]; then
  echo "A source file is required."
  print_usage
fi

# output given path relative to cwd
# We use the fn under the assumption that cwd is our repo root
function relpath {
  local fullpath=$(cd $(dirname $1); pwd)/$(basename $1);
  local parent="$(dirname "$(pwd)")"
  # sed s,pattern,replacement,s -- using , form instead of / form
  # to avoid having to escape /
  echo $fullpath | sed 's,'"$parent,"','
}

echo "Will use this css file"
ls "$cssfile"

files="$sourcefile"

echo

for p in $files
do
  # Make put code in markdown tempfile
  f="$(basename $p)"
  md_outfile="${f%%.*}.md"
  echo "---" > $md_outfile
  echo "title:  $f" >> $md_outfile
  echo "---" >> $md_outfile
  echo >> $md_outfile
  echo "\`\`\`${f%%*.}" >> $md_outfile
  cat $p >> $md_outfile
  echo >> $md_outfile
  echo "\`\`\`" >> $md_outfile
  echo >> $md_outfile

  # Make html for footer
  echo "<footer>" > $footerfile
  #echo "<p>Last update: <time>$(date +"%Y-%m-%d %T")</time></p>" >> $footerfile
  echo "<p>Last update: $(git log --pretty="%ai (%an - %h)" -1 $p)</p>" >> $footerfile
  echo "<p>Source: $(relpath $p)</p>" >> $footerfile
  echo "</footer>" >> $footerfile

  echo "Will append this footer:"
  cat $footerfile
  echo "Converting $p"
  # if undefined, give outfile default value of <sourcefile>.html
  if [[ -z "$outfile" ]]; then
    f="$(basename $p)"
    p_outfile="${f%%.*}.html"
  fi

  # if undefined, set the destination to the parent of the sourcefile
  if [[ -z "$outdir" ]]; then
    destination="$(dirname $p)/$p_outfile"
    # note, --self-contained implies no need for --extract-media in pandoc
    media_dir=temp/assets
  else
    # assuming p is a path relative to repo root
    mkdir -p $outdir/$(dirname $p)
    destination="$outdir/$(dirname $p)/$p_outfile"
    media_dir=$outdir/assets
  fi

  echo "Sending output to $destination"

  # convert to html
  echo pandoc "$md_outfile" -f markdown -t html5 -o "$destination" -A "$footerfile" --self-contained \
  --css "$cssfile" 
  pandoc "$md_outfile" -f markdown -t html5 -o "$destination" -A "$footerfile" --self-contained \
  --css "$cssfile"
  
  rm $md_outfile
done

# clean intermediate working files
rm -f $footerfile
