#!/bin/bash
set -eo pipefail

# Given a rst file and a css file, generate html.
# The markdown is converted to an HTML document, and then that is output to pdf.
# Tested on Windows 10 in git-bash with pandoc 2.11.3.2
# Script written under the assumption that it's called from the root of
# the project repository (to compute relative paths in the footer)

rel_script_dir="$(dirname $0)"
cssfile="$rel_script_dir/gh_pandoc_rst.css"
# temporary files
footerfile='pandoc_footer.html'

function print_usage {
  echo Usage: "$0 -i <path/to/source.rst> [-o <target.html>] [-c <path/to/css>] [-d <path/to/outdir>] [-m <path/to/manifest>]"
  exit 1
}

# need at least one argument, -i option
if [[ $# -lt 2 ]]; then
  print_usage
fi

while [[ $# -gt 1 ]]
do
  key="$1"
  case $key in
      -i|--in)
      sourcefile="$2"
      shift
      ;;
      -o|--out)
        outfile="$2"
        shift
        ;;
      -c|--css)
        cssfile="$2"
        shift
        ;;
      -d|--out-dir)
        outdir="$2"
        shift
        ;;
      -m|--manifest)
        manifest="$2"
        shift
        ;;
      -h|--help)
        print_usage
        ;;
      *)
        print_usage
        ;;
  esac
  shift
done

if [[ -z "$sourcefile" && -z "$manifest" ]]; then
  echo "A source file or a manifest are required."
  print_usage
fi

# output given path relative to cwd
# We use the fn under the assumption that cwd is our repo root
function relpath {
  fullpath=$(cd $(dirname $1); pwd)/$(basename $1);
  parent="$(dirname "$(pwd)")"
  # sed s,pattern,replacement,s -- using , form instead of / form
  # to avoid having to escape /
  echo $fullpath | sed 's,'"$parent,"','
}

echo "Will use this css file"
ls "$cssfile"

files="$sourcefile"
if [[ "$manifest" ]]; then
  files=$(grep -Ev "^#|^$" "$manifest")
fi

echo

for p in $files
do
  # Make html for footer
  echo "<footer>" > $footerfile
  #echo "<p>Last update: <time>$(date +"%Y-%m-%d %T")</time></p>" >> $footerfile
  echo "<p>Last update: $(git log --pretty="%ai (%an - %h)" -1 $p)</p>" >> $footerfile
  echo "<p>Source: $(relpath $p)</p>" >> $footerfile
  echo "</footer>" >> $footerfile

  echo "Will append this footer:"
  cat $footerfile
  echo "Converting $p"
  # if undefined, give outfile default value of <sourcefile>.html
  if [[ -z "$outfile" ]]; then
    f="$(basename $p)"
    p_outfile="${f%%.*}.html"
  fi

  # if undefined, set the outdir to the parent of the sourcefile
  if [[ -z "$outdir" ]]; then
    destination="$(dirname $p)/$p_outfile"
  else
    # assuming p is a path relative to repo root
    mkdir -p $outdir/$(dirname $p)
    destination="$outdir/$(dirname $p)/$p_outfile"
  fi

  echo "Sending output to $destination"

  # convert to html
  echo pandoc "$p" -f rst -t html5 -o "$destination" -A "$footerfile" --self-contained \
  --css "$cssfile" --toc
  pandoc "$p" -f rst -t html5 -o "$destination" -A "$footerfile" --self-contained \
  --css "$cssfile" --toc
done

# clean intermediate working files
rm -f $footerfile
