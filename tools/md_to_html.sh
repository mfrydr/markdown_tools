#!/bin/bash
set -eo pipefail

# Given a md file and a css file, generate html.
# The markdown is converted to an HTML document, and then that is output to pdf.
# Tested on Windows 10 in git-bash with pandoc 2.11.3.2
# Script written under the assumption that it's called from the root of
# the project repository (to compute relative paths in the footer)

rel_script_dir="$(dirname $0)"
cssfile="$rel_script_dir/gh_pandoc.css"
# temporary files
footerfile="pandoc_footer.html"
# generate a table of contents by default
toc_arg="--toc"

function print_usage {
  echo Usage: "$0 -i <path/to/source.md> [-o <target.html>] [-c <path/to/css>] [-d <path/to/outdir>] [-m <path/to/manifest>] [--no-toc]"
  exit 1
}

function check_arg {
  echo check_arg $1 $2
  if [[ -z "$2" || "$2" =~ ^- ]]; then
    echo "Missing argument for $1"
    print_usage
    exit 1
  fi
}

# need at least one argument, -i option
if [[ $# -lt 2 ]]; then
  print_usage
fi

while [[ $# -ge 1 ]]
do
  key="$1"
  echo $key
  case $key in
      -i|--in)
      check_arg "$key" "$2"
      sourcefile="$2"
      shift
      ;;
      -o|--out)
        check_arg "$key" "$2"
        outfile="$2"
        shift
        ;;
      -c|--css)
        check_arg "$key" "$2"
        cssfile="$2"
        shift
        ;;
      -d|--out-dir)
        check_arg "$key" "$2"
        outdir="$2"
        shift
        ;;
      -m|--manifest)
        check_arg "$key" "$2"
        manifest="$2"
        shift
        ;;
      --no-toc)
        toc_arg=""
        ;;
      -h|--help)
        print_usage
        ;;
      *)
        print_usage
        ;;
  esac
  shift
done

if [[ -z "$sourcefile" && -z "$manifest" ]]; then
  echo "A source file or a manifest are required."
  print_usage
fi

# output given path relative to cwd
# We use the fn under the assumption that cwd is our repo root
function relpath {
  local fullpath=$(cd $(dirname $1); pwd)/$(basename $1);
  local parent="$(dirname "$(pwd)")"
  # sed s,pattern,replacement,s -- using , form instead of / form
  # to avoid having to escape /
  echo $fullpath | sed 's,'"$parent,"','
}

echo "Will use this css file"
ls "$cssfile"

files="$sourcefile"
if [[ "$manifest" ]]; then
  files=$(grep -Ev "^#|^$" "$manifest")
fi

echo

for p in $files
do
  # Make html for footer
  echo "<footer>" > $footerfile
  #echo "<p>Last update: <time>$(date +"%Y-%m-%d %T")</time></p>" >> $footerfile
  echo "<p>Last update: $(git log --pretty="%ai (%an - %h)" -1 $p)</p>" >> $footerfile
  echo "<p>Source: $(relpath $p)</p>" >> $footerfile
  echo "</footer>" >> $footerfile

  echo "Will append this footer:"
  cat $footerfile
  echo "Converting $p"
  # if undefined, give outfile default value of <sourcefile>.html
  if [[ -z "$outfile" ]]; then
    f="$(basename $p)"
    p_outfile="${f%%.*}.html"
  fi

  # if undefined, set the destination to the parent of the sourcefile
  if [[ -z "$outdir" ]]; then
    destination="$(dirname $p)/$p_outfile"
    # note, --embed-resources --standalone implies no need for --extract-media in pandoc
    media_dir=temp/assets
  else
    # assuming p is a path relative to repo root
    mkdir -p $outdir/$(dirname $p)
    destination="$outdir/$(dirname $p)/$p_outfile"
    media_dir=$outdir/assets
  fi

  echo "Sending output to $destination"

  # convert to html
  echo pandoc "$p" -f markdown -t html5 -o "$destination" -A "$footerfile" --embed-resources --standalone \
  --css "$cssfile" $toc_arg
  pandoc "$p" -f markdown -t html5 -o "$destination" -A "$footerfile" --embed-resources --standalone \
  --css "$cssfile" $toc_arg
done

# clean intermediate working files
rm -f $footerfile
