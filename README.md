# markdown_tools

Tools, scripts and config used to publish markdown files or source code as HTML or PDF

Include this as a submodule in other repos.

Example repo structure:

```
 .
  \
   - markdown_tools (submodule)
   - src (markdown files)
   - .manifest (list of files to publish)
   - .gitlab-ci.yml
```
